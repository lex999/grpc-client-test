package org.example;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8090")
                .usePlaintext().build();

        com.service.grpc.ImageReadServiceGrpc.ImageReadServiceBlockingStub stub = com.service.grpc.ImageReadServiceGrpc
                .newBlockingStub(channel);

        com.service.grpc.ImageService.GetImageRequest request = com.service.grpc.ImageService.GetImageRequest
                .newBuilder().setId("Id1").build();

        com.service.grpc.ImageService.GetImageResponse response = stub.getImageByReq(request);
        System.out.println(response);
        channel.shutdown();
    }
}
